import 'package:vkstats/exception/login_failed_exception.dart';

class IncorrectLoginException extends LoginFailedException {
}