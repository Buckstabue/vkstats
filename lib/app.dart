import 'package:flutter/material.dart';
import 'package:vkstats/ui/widget/auth_screen.dart';
import 'package:vkstats/ui/widget/stats_screen.dart';

void main() => runApp(new App());

class App extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        home: new Scaffold(body: new AuthScreenWidget()),
        routes: {
          '/stats': (BuildContext context) => new Scaffold(body: new StatsScreenWidget())
        }
    );
  }
}
