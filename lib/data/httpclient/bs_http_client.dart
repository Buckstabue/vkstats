import 'dart:convert';

import 'package:collection/src/wrappers.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'dart:async';
import 'package:http/src/response.dart';
import 'basic_http_client.dart';
import 'package:flutter/foundation.dart';

class BsHttpClient extends BasicHttpClient {
  final String _HEADER_LOCATION = "location";
  final String _HEADER_SET_COOKIE = "set-cookie";
  final String _HEADER_COOKIE = "Cookie";
  final String _HEADER_CONNECTION = "Connection";
  final String _HEADER_CONNECTION_DEFAULT_VALUE = "close";
  final String _HEADER_ACCEPT = "Accept";
  final String ROOT_URL = "https://m.vk.com";


  var _httpClient = createHttpClient();

  final bool autoRedirect;
  final bool saveCookies;

  final Map<String, String> _cookieMap = {};

  BsHttpClient({this.autoRedirect, this.saveCookies});


  @override
  void clearCookies() {
    _cookieMap.clear();
  }

  @override
  Future<Response> get(String url, {Map<String, String> headers}) {
    if (url.startsWith("/")) {
      url = ROOT_URL + url;
    }
    debugPrint("GET url=$url");
    Map<String, String> headersCopy = new Map.from(headers);
    _addAcceptHeaderIfNeeded(headersCopy);
    _addConnectionHeaderIfUndefined(headersCopy);
    _insertCookies(headersCopy);
    return _internalGet(url, headers: headersCopy)
        .then((response) {
      _processCookies(response.headers);
      return response;
    }).then((response) {
      return _handleRedirectIfNeeded(response, headers, url);
    });
  }

  Future<Response> _internalGet(url, {Map<String, String> headers}) =>
      _sendUnstreamed("GET", url, headers);

  void _addAcceptHeaderIfNeeded(Map headersCopy) {
    headersCopy.putIfAbsent(_HEADER_ACCEPT, () => "*/*");
  }

  void _addConnectionHeaderIfUndefined(Map<String, String> headers) {
    headers.putIfAbsent(_HEADER_CONNECTION, () => _HEADER_CONNECTION_DEFAULT_VALUE);
  }

  @override
  Future<Response> post(String url, {Map<String, String> headers, Map<String, String> body}) {
    debugPrint("POST url=$url");
    var headersCopy = new Map.from(headers);
    _addConnectionHeaderIfUndefined(headersCopy);
    _addAcceptHeaderIfNeeded(headersCopy);
    _insertCookies(headersCopy);
    return _internalPost(url, headers: headersCopy, body: body)
        .then((response) {
      _processCookies(response.headers);
      return response;
    }).then((response) {
      return _handleRedirectIfNeeded(response, headers, url);
    });
  }

  Future<Response> _internalPost(url, {Map<String, String> headers, body,
    Encoding encoding}) =>
      _sendUnstreamed("POST", url, headers, body, encoding);

  void _insertCookies(Map<String, String> map) {
    var params = <String>[];
    _cookieMap.forEach((key, value) {
      String param = "$key=${Uri.encodeComponent(value)}";
      params.add(param);
    });
    var cookieHeaderValue = params.join(";");
    if (cookieHeaderValue.isNotEmpty) {
      map[_HEADER_COOKIE] = cookieHeaderValue;
    }
    debugPrint("insert cookies: $cookieHeaderValue");
  }

  void _processCookies(Map<String, String> headers) {
    if (!saveCookies) {
      return;
    }
    headers.forEach((header, value) {
      if (header == _HEADER_SET_COOKIE) {
        List<_StringPair> cookieValues = _parseCookieHeaders(value);
        cookieValues.forEach((pair) {
          _cookieMap[pair.first] = pair.second;
        });
      }
    });
  }

  Future<Response> _handleRedirectIfNeeded(Response response, Map<String, String> headers, String urlFrom) {
    if (!autoRedirect) {
      return new Future.value(response);
    }
    if (response.statusCode == 302) {
      String locationUrl = response.headers[_HEADER_LOCATION];
      debugPrint("REDIRECT url=$locationUrl");
      return get(locationUrl, headers: headers);
    }
    return new Future.value(response);
  }

  List<_StringPair> _parseCookieHeaders(String header) {
    debugPrint("parsing cookies: $header");
    var result = <_StringPair>[];
    List<String> cookieSections = header.split(",");
    cookieSections.removeWhere((element) => element.startsWith(new RegExp(r"\s")));
    cookieSections.forEach((cookieSection) {
      result.add(_parseCookieSection(cookieSection));
    });
    return result;
  }

  _StringPair _parseCookieSection(String cookieSection) {
    var semicolonPos = cookieSection.indexOf(";");
    var cookieValueSubstring = cookieSection.substring(0, semicolonPos);
    List<String> split = cookieValueSubstring.split('=');
    assert (split.length == 2);
    return new _StringPair(split[0], split[1]);
  }


  Future<Response> _sendUnstreamed(String method, url,
      Map<String, String> headers, [body, Encoding encoding]) async {
    if (url is String) url = Uri.parse(url);
    var request = new Request(method, url);

    if (headers != null) request.headers.addAll(headers);
    if (encoding != null) request.encoding = encoding;
    if (body != null) {
      if (body is String) {
        request.body = body;
      } else if (body is List) {
        request.bodyBytes = DelegatingList.typed(body);
      } else if (body is Map) {
        request.bodyFields = DelegatingMap.typed(body);
      } else {
        throw new ArgumentError('Invalid request body "$body".');
      }
    }
    request.followRedirects = false;

    return Response.fromStream(await _httpClient.send(request));
  }
}

class _StringPair {
  final String first;
  final String second;

  _StringPair(this.first, this.second);
}
