import 'dart:async';
import 'package:http/src/response.dart';

abstract class BasicHttpClient {
  void clearCookies();
  Future<Response> get(String url, {Map<String, String> headers});
  Future<Response> post(String url, {Map<String, String> headers, Map<String, String> body});
}