import 'dart:async';
import 'package:vkstats/domain/datainterface/api/vk_api_interface.dart';
import 'package:vkstats/exception/incorrect_login_exception.dart';
import 'package:vkstats/exception/login_failed_exception.dart';
import 'package:vkstats/exception/phone_protect_detected_exception.dart';
import 'package:vkstats/data/httpclient/basic_http_client.dart';
import 'package:flutter/foundation.dart';

class VkApiImpl extends VkApiInterface {

  final String DEFAULT_HOST_URL = "https://m.vk.com";
  final String ATTR_LOGIN = "email";
  final String ATTR_PASSWORD = "pass";

  final String HEADER_USER_AGENT = "User-agent";
  final String MOBILE_USER_AGENT = "Mozilla/5.0 (Linux; Android 7.0; PLUS Build/NRD90M) AppleWebKit/537.36" +
      " (KHTML, like Gecko) Chrome/61.0.3163.98 Mobile Safari/537.36";
  final String DESKTOP_USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0";

  final RegExp loginActionRegex = new RegExp(r'action="(http.+?)"');

  final BasicHttpClient httpClient;

  VkApiImpl(this.httpClient);

  @override
  Stream<Null> login(String login, String password) {
    StreamController<Null> streamController;
    streamController = new StreamController<Null>(
        onListen: () => _doLogin(login, password, streamController)
    );
    return streamController.stream;
  }

  Future<Null> _doLogin(String login, String password, StreamController<Null> streamController) async {
    httpClient.clearCookies();
    Map<String, String> headers = {HEADER_USER_AGENT: MOBILE_USER_AGENT};
    httpClient.get(DEFAULT_HOST_URL, headers: headers).then((response) {
      var body = response.body.toString();
      return extractLoginActionUrl(body);
    }).then((loginActionUrl) {
      var postParams = {ATTR_LOGIN: login, ATTR_PASSWORD: password};
      return httpClient.post(loginActionUrl, body: postParams, headers: headers);
    }).then((response) {
      String body = response.body.toString();
      if (isLoginFailed(body)) {
        streamController.addError(new IncorrectLoginException());
      } else if (isPhoneProtected(body)) {
        String actionUrl = _getPhoneProtectedCode(body);
        if (actionUrl.startsWith("/")) {
          actionUrl = DEFAULT_HOST_URL + actionUrl;
        }
        streamController.addError(new PhoneProtectDetectedException(actionUrl));
        return;
      } else {
        if (_isSignedIn(body)) {
          debugPrint("Login Done!");
          debugPrint(body);
          streamController.close();
        } else {
          streamController.addError(new LoginFailedException());
        }
      }
    }).catchError((error) => streamController.addError(error));
  }

  bool _isSignedIn(String html) {
    return html.contains("settings") && html.contains("logout") && html.contains("friends") && html.contains("mail");
  }


  @override
  Stream<Null> sendSecureCode(String actionUrl, String code, bool rememberMe) {
    StreamController<Null> streamController;
    streamController = new StreamController<Null>(
        onListen: () => _internalSendSecureCode(actionUrl, code, rememberMe, streamController)
    );
    return streamController.stream;
  }

  void _internalSendSecureCode(String actionUrl, String code, bool rememberMe,
      StreamController<Null> streamController) {
    Map<String, String> headers = {HEADER_USER_AGENT: MOBILE_USER_AGENT};
    Map<String, String> postParams = {
      "code": code,
      "remember": rememberMe ? "1" : "0"
    };
    httpClient.post(actionUrl, headers: headers, body: postParams)
        .then((response) {
      String html = response.body.toString();
      if (isPhoneProtected(html)) {
        streamController.addError(new PhoneProtectDetectedException(_getPhoneProtectedCode(html)));
      } else if (_isSignedIn(html)) {
        streamController.close();
      } else {
        streamController.addError(new LoginFailedException());
      }
    }).catchError((error) => streamController.addError(error));
  }

  String extractLoginActionUrl(String body) {
    var firstMatch = loginActionRegex.firstMatch(body);
    if (firstMatch == null) {
      // TODO add error handling
    }
    return firstMatch.group(1);
  }

  bool isPhoneProtected(String body) {
    return body.contains("act=authcheck_code");
  }

  String _getPhoneProtectedCode(String body) {
    return new RegExp(r'action="(.*?/login\?act=authcheck_code.+?)"').firstMatch(body).group(1);
  }

  bool isLoginFailed(String body) {
    return body.contains("login.vk.com") && body.contains("email") && body.contains("pass");
  }
}