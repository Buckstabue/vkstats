import 'base_view.dart';

abstract class AuthView implements BaseView {
  void showError(String message);

  void showEnterAuthCodeDialog(String actionUrl);
  void showStatsScreen();
}
