import 'package:flutter/widgets.dart';
import 'package:vkstats/exception/illegal_state_exception.dart';


class LineChartPainter extends CustomPainter {
  final int xStart;
  final int yStart;

  /// how much pixels 1 point is for the X axe
  final int xZoom;

  /// how much pixels 1 point is for the Y axe
  final int yZoom;

  final List<LineChartDataSet> dataSets;
  final Map<LineChartStyle, Paint> _paints = {};
  final XAnchor xAnchor;

  LineChartPainter(this.dataSets, this.xStart, this.yStart, this.xZoom, this.yZoom, this.xAnchor) {
    dataSets.forEach((dataSet) {
      _paints.putIfAbsent(dataSet.style, () {
        var paint = new Paint();
        paint.color = dataSet.style.color;
        paint.strokeWidth = dataSet.style.lineWidth;
        paint.style = PaintingStyle.stroke;
        return paint;
      });
    });
  }


  @override
  void paint(Canvas canvas, Size size) {
    canvas.translate(xStart.toDouble(), size.height);
    canvas.scale(1.0, -1.0);

    for (var dataSet in dataSets) {
      int rightElement = calculateRightElement(dataSet, size.width);
      int leftElement = calculateLeftElement(dataSet, size.width);
      var paint = _paints[dataSet.style];
      var drawPath = new Path();
      drawPath.moveTo(0.0, 0.0);
      for (int i = leftElement; i < rightElement && i < dataSet.points.length; ++i) {
        var point = dataSet.points[i];
        var drawX = point.x * xZoom - xStart;
        var drawY = point.y * yZoom;
        drawPath.lineTo(drawX, drawY);
      }
      canvas.drawPath(drawPath, paint);
    }
  }

  @override
  bool shouldRepaint(LineChartPainter oldDelegate) {
    return this.xStart != oldDelegate.xStart || this.yStart != oldDelegate.yStart ||
        this.dataSets != oldDelegate.dataSets;
  }

  int calculateRightElement(LineChartDataSet dataSet, double width) {
    switch (xAnchor) {
      case XAnchor.RIGHT:
        return xStart * xZoom;
      case XAnchor.LEFT:
        return ((xStart * xZoom) + (width / xZoom)).toInt();
    }
    throw new IllegalStateException();
  }

  int calculateLeftElement(LineChartDataSet dataSet, double width) {
    switch (xAnchor) {
      case XAnchor.RIGHT:
        return ((xStart * xZoom) + (width / xZoom)).toInt();
      case XAnchor.LEFT:
        return xStart * xZoom;
    }
    throw new IllegalStateException();
  }
}

abstract class YDescriptionProvider {
  List<YDescription> provideDescriptions(int minY, int maxY);
}

abstract class XDescriptionProvider {
  List<XDescription> provideDescriptions(int minX, int maxX);
}

class YDescription {
  final int yValue;
  final String label;

  YDescription(this.yValue, this.label);
}

class XDescription {
  final int xValue;
  final String label;

  XDescription(this.xValue, this.label);
}

class ChartPoint {
  final double x;
  final double y;

  ChartPoint(this.x, this.y);
}

class LineChartDataSet {
  final List<ChartPoint> points;
  final LineChartStyle style;

  LineChartDataSet(this.points, this.style);
}

class LineChartStyle {
  final Color color;
  final double lineWidth;

  LineChartStyle(this.color, this.lineWidth);
}

enum XAnchor {
  RIGHT,
  LEFT
}
