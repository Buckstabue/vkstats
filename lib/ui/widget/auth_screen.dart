import 'package:flutter/material.dart';
import 'package:vkstats/ui/presenter/auth_presenter.dart';
import 'package:vkstats/ui/view/auth_view.dart';

import 'package:vkstats/domain/interactor/login_interactor.dart';
import 'package:vkstats/domain/interactor/send_secure_code_interactor.dart';
import 'package:vkstats/data/api/vk_api_impl.dart';
import 'package:vkstats/data/httpclient/bs_http_client.dart';

class AuthScreenWidget extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => new _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreenWidget> implements AuthView {
  final TextEditingController loginController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();

  AuthPresenter _authPresenter;

  _AuthScreenState() {
    var vkApiImpl = new VkApiImpl(new BsHttpClient(autoRedirect: true, saveCookies: true)); // FIXME use DI
    var loginInteractor = new LoginInteractor(vkApiImpl);
    var sendSecureCodeInteractor = new SendSecureCodeInteractor(vkApiImpl);
    _authPresenter = new AuthPresenter(this, loginInteractor, sendSecureCodeInteractor);
  }


  @override
  void initState() {
    loginController.text = "+79775754300";
    passwordController.text = "fateslav4455";
//    loginController.text = "+79038574455";
//    passwordController.text = "ktotam";
  }

  @override
  Widget build(BuildContext context) =>
      new Scaffold(
          appBar: new AppBar(
              title: new Text("VkStats")),
          body: new Builder(builder: (BuildContext context) {
            return new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  new Text("Логин"),
                  new TextField(controller: loginController),
                  new Text("Пароль"),
                  new TextField(controller: passwordController),
                  new RaisedButton(
                      onPressed: () =>
                          _authPresenter.onLoginButtonClicked(loginController.text, passwordController.text),
                      child: new Text("Войти")
                  )
                ]
            );
          })
      );

  @override
  void showError(String message) {
    debugPrint("error: $message");
    Scaffold.of(context)
        .showSnackBar(new SnackBar(content: new Text(message)));
  }

  @override
  void showEnterAuthCodeDialog(String actionUrl) {
    var smsController = new TextEditingController();

    showDialog<String>(context: context, barrierDismissible: true,
        child: new AlertDialog(title: const Text("Введите SMS код:"),
            content: new TextField(
                controller: smsController,
                keyboardType: TextInputType.number,
                autocorrect: false,
                autofocus: true
            ),
            actions: [
              new FlatButton(
                  onPressed: () => Navigator.pop(context, null),
                  child: const Text("ОТМЕНА")),
              new FlatButton(
                  onPressed: () => Navigator.pop(context, smsController.text),
                  child: const Text("ОК"))
            ]
        )
    ).then((String input) {
      if (input == null) {
        _authPresenter.onEnteringSmsCodeCancelled();
      } else {
        _authPresenter.onSecureSmsCodeEntered(input, actionUrl);
      }
    });
  }

  @override
  void showStatsScreen() {
    Navigator.of(context).pushReplacementNamed('/stats');
  }


}
