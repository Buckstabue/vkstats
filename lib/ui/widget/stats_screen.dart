import 'package:flutter/material.dart';
import 'package:vkstats/ui/widget/custom/line_chart.dart';

class StatsScreenWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _StatsScreenState();
}

class _StatsScreenState extends State<StatsScreenWidget> {
  LineChartDataSet _dataSet;

  _StatsScreenState() {
    List<ChartPoint> points = [new ChartPoint(0.0, 0.0), new ChartPoint(1.0, 1.0), new ChartPoint(2.0, 2.0),
    new ChartPoint(3.0, 1.0), new ChartPoint(4.0, 0.0)
    ];
    _dataSet = new LineChartDataSet(points, new LineChartStyle(Colors.blue, 5.0));
  }


  @override
  Widget build(BuildContext context) {
    return new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new CustomPaint(
            painter: new LineChartPainter([_dataSet], 0, 0, 50, 50, XAnchor.LEFT)
          )
        ]
    );
  }
}
