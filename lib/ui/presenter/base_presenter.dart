import 'package:vkstats/ui/view/base_view.dart';

class BasePresenter<V extends BaseView> {
  V view;

  BasePresenter(this.view);
}