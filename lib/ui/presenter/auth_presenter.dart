import 'package:vkstats/exception/incorrect_login_exception.dart';
import 'package:vkstats/exception/login_failed_exception.dart';
import 'package:vkstats/exception/phone_protect_detected_exception.dart';

import 'base_presenter.dart';
import 'package:vkstats/ui/view/auth_view.dart';
import 'package:vkstats/exception/validation_exception.dart';
import 'package:vkstats/domain/interactor/login_interactor.dart';
import 'package:vkstats/domain/interactor/send_secure_code_interactor.dart';
import 'package:flutter/foundation.dart';

class AuthPresenter extends BasePresenter<AuthView> {
  final LoginInteractor loginInteractor;
  final SendSecureCodeInteractor secureCodeInteractor;

  AuthPresenter(AuthView view, this.loginInteractor, this.secureCodeInteractor) :
        super(view);

  void onLoginButtonClicked(String login, String password) {
    { // TODO remove it!
      _showStatsScreen();
      return;
    }
    try {
      _validateLogin(login);
      _validatePassword(password);
    } on ValidationException catch (e) {
      view.showError(e.message);
      return;
    }
    _doLogin(login, password);
  }

  void _validateLogin(String login) {
    if (login
        .trim()
        .isEmpty) {
      throw new ValidationException("Введите логин");
    }
  }

  void _validatePassword(String password) {
    if (password
        .trim()
        .isEmpty) {
      throw new ValidationException("Введите пароль");
    }
  }

  void _doLogin(String login, String password) {
    loginInteractor.execute(login, password).listen(
        null,
        onDone: () => _showStatsScreen(),
        onError: (err) {
          if (err is IncorrectLoginException) {
            String errorMessage = "Неверный логин/пароль";
            view.showError(errorMessage);
          } else if (err is PhoneProtectDetectedException) {
            view.showEnterAuthCodeDialog(err.actionUrl);
            return;
          } else if (err is LoginFailedException) {
            String errorMessage = "Не удалось войти по неизвестной причине";
            view.showError(errorMessage);
          } else {
            String errorMessage = err.toString();
            view.showError(errorMessage);
          }
        });
  }

  void _showStatsScreen() {
    view.showStatsScreen();
  }

  void onEnteringSmsCodeCancelled() {
    debugPrint("User cancelled to enter sms code");
  }

  void onSecureSmsCodeEntered(String code, String actionUrl) {
    secureCodeInteractor.execute(actionUrl, code, rememberMe: true)
        .listen(null,
        onDone: () {
          debugPrint("Login done!");
          _showStatsScreen();
        },
        onError: (err) {
          if (err is PhoneProtectDetectedException) {
            view.showError("Введен неверный код");
          } else if (err is LoginFailedException) {
            String errorMessage = "Не удалось войти по неизвестной причине";
            view.showError(errorMessage);
          } else {
            String errorMessage = err.toString();
            view.showError(errorMessage);
          }
        }
    );
  }
}
