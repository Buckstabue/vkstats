import 'dart:async';

abstract class VkApiInterface {

  /// May produce IncorrectLoginException or PhoneProtectDetectedException
  Stream<Null> login(String login, String password);

  /// May produce PhoneProtectDetectedException if secure code is invalid
  Stream<Null> sendSecureCode(String actionUrl, String code, bool rememberMe);
}
