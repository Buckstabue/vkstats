import 'dart:async';
import 'package:vkstats/domain/datainterface/api/vk_api_interface.dart';

class LoginInteractor {
  final VkApiInterface _vkApiInterface;

  LoginInteractor(this._vkApiInterface);

  Stream<Null> execute(String login, String password) {
    return _vkApiInterface.login(login, password);
  }
}