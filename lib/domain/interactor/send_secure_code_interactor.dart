import 'dart:async';

import 'package:vkstats/domain/datainterface/api/vk_api_interface.dart';

class SendSecureCodeInteractor {
  final VkApiInterface _vkApiInterface;

  SendSecureCodeInteractor(this._vkApiInterface);

  Stream<Null> execute(String actionUrl, String code, {bool rememberMe}) {
    return _vkApiInterface.sendSecureCode(actionUrl, code, rememberMe);
  }
}